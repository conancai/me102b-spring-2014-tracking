package com.me102b.tracking;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import android.content.Context;
import android.content.Intent;
import android.widget.TextView;
import com.hoho.android.usbserial.driver.UsbSerialDriver;
import com.hoho.android.usbserial.util.HexDump;
import com.hoho.android.usbserial.util.SerialInputOutputManager;
import org.opencv.android.BaseLoaderCallback;
import org.opencv.core.Point;
import org.opencv.core.Scalar;
import org.opencv.android.CameraBridgeViewBase.CvCameraViewFrame;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.Rect;
import org.opencv.core.Size;
import org.opencv.android.CameraBridgeViewBase;
import org.opencv.android.CameraBridgeViewBase.CvCameraViewListener2;
import org.opencv.imgproc.Imgproc;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnTouchListener;

public class ColorBlobDetectionActivity extends Activity implements OnTouchListener, CvCameraViewListener2 {
    private static final String  TAG              = "OCVSample::Activity";

    private boolean              mIsColorSelected = false;
    private Mat                  mRgba;
    private Scalar               mBlobColorRgba;
    private Scalar               mBlobColorHsv;
    private ColorBlobDetector    mDetector;
    private Mat                  mSpectrum;
    private Size                 SPECTRUM_SIZE;
    private Scalar               CONTOUR_COLOR;
    private Scalar				 CENTRAL_COLOR;
    
    private CameraBridgeViewBase mOpenCvCameraView;

    private BaseLoaderCallback mLoaderCallback = new BaseLoaderCallback(this) {
        @Override
        public void onManagerConnected(int status) {
            switch (status) {
                case LoaderCallbackInterface.SUCCESS:
                {
                    Log.i(TAG, "OpenCV loaded successfully");
                    mOpenCvCameraView.enableView();
                    mOpenCvCameraView.setOnTouchListener(ColorBlobDetectionActivity.this);
                } break;
                default:
                {
                    super.onManagerConnected(status);
                } break;
            }
        }
    };

    public ColorBlobDetectionActivity() {
        Log.i(TAG, "Instantiated new " + this.getClass());
    }

    //USB SERIAL
    int currX = 0;
    int currY = 0;
    int targX = 0;
    int targY = 0;
    boolean first = true;
    private static UsbSerialDriver sDriver = null;
    private TextView pitchText;
    private TextView yawText;
    private TextView modeText;
    private TextView titleText;
    private SerialInputOutputManager mSerialIoManager;
    private final ExecutorService mExecutor = Executors.newSingleThreadExecutor();

    private final SerialInputOutputManager.Listener mListener =
            new SerialInputOutputManager.Listener() {

                @Override
                public void onRunError(Exception e) {
                    Log.d(TAG, "Runner stopped.");
                }

                @Override
                public void onNewData(final byte[] data) {
                    ColorBlobDetectionActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            ColorBlobDetectionActivity.this.updateReceivedData(data);
                        }
                    });
                }
            };
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.i(TAG, "called onCreate");
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        setContentView(R.layout.color_blob_detection_surface_view);

        mOpenCvCameraView = (CameraBridgeViewBase) findViewById(R.id.color_blob_detection_activity_surface_view);
        mOpenCvCameraView.setCvCameraViewListener(this);

        pitchText = (TextView) findViewById(R.id.pitch);
        yawText = (TextView) findViewById(R.id.yaw);
        modeText = (TextView) findViewById(R.id.mode);
        titleText = (TextView) findViewById(R.id.title);
    }

    @Override
    public void onPause()
    {
        super.onPause();
        if (mOpenCvCameraView != null)
            mOpenCvCameraView.disableView();
        stopIoManager();
        if (sDriver != null) {
            try {
                sDriver.close();
            } catch (IOException e) {
                // Ignore.
            }
            sDriver = null;
        }
        finish();
    }

    @Override
    public void onResume()
    {
        super.onResume();
        OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_2_4_3, this, mLoaderCallback);
        Log.d(TAG, "Resumed, sDriver=" + sDriver);
        if (sDriver == null) {
            titleText.setText("No serial device.");
        } else {
            try {
                sDriver.open();
                sDriver.setParameters(115200, 8, UsbSerialDriver.STOPBITS_1, UsbSerialDriver.PARITY_NONE);
            } catch (IOException e) {
                Log.e(TAG, "Error setting up device: " + e.getMessage(), e);
                titleText.setText("Error opening device: " + e.getMessage());
                try {
                    sDriver.close();
                } catch (IOException e2) {
                    // Ignore.
                }
                sDriver = null;
                return;
            }
            titleText.setText("Serial device: " + sDriver.getClass().getSimpleName());
        }
        onDeviceStateChange();
    }

    public void onDestroy() {
        super.onDestroy();
        if (mOpenCvCameraView != null)
            mOpenCvCameraView.disableView();
    }

    public void onCameraViewStarted(int width, int height) {
        mRgba = new Mat(height, width, CvType.CV_8UC4);
        mDetector = new ColorBlobDetector();
        mSpectrum = new Mat();
        mBlobColorRgba = new Scalar(255);
        mBlobColorHsv = new Scalar(255);
        SPECTRUM_SIZE = new Size(200, 64);
        CONTOUR_COLOR = new Scalar(255,0,0,255);
        CENTRAL_COLOR = new Scalar(0,255,0,255);
    }

    public void onCameraViewStopped() {
        mRgba.release();
    }

    public boolean onTouch(View v, MotionEvent event) {
        int cols = mRgba.cols();
        int rows = mRgba.rows();

        int xOffset = (mOpenCvCameraView.getWidth() - cols) / 2;
        int yOffset = (mOpenCvCameraView.getHeight() - rows) / 2;

        int x = (int)event.getX() - xOffset;
        int y = (int)event.getY() - yOffset;

        Log.i(TAG, "Touch image coordinates: (" + x + ", " + y + ")");

        if ((x < 0) || (y < 0) || (x > cols) || (y > rows)) return false;

        Rect touchedRect = new Rect();

        touchedRect.x = (x>4) ? x-4 : 0;
        touchedRect.y = (y>4) ? y-4 : 0;

        touchedRect.width = (x+4 < cols) ? x + 4 - touchedRect.x : cols - touchedRect.x;
        touchedRect.height = (y+4 < rows) ? y + 4 - touchedRect.y : rows - touchedRect.y;

        Mat touchedRegionRgba = mRgba.submat(touchedRect);

        Mat touchedRegionHsv = new Mat();
        Imgproc.cvtColor(touchedRegionRgba, touchedRegionHsv, Imgproc.COLOR_RGB2HSV_FULL);

        // Calculate average color of touched region
        mBlobColorHsv = Core.sumElems(touchedRegionHsv);
        int pointCount = touchedRect.width*touchedRect.height;
        for (int i = 0; i < mBlobColorHsv.val.length; i++)
            mBlobColorHsv.val[i] /= pointCount;

        mBlobColorRgba = converScalarHsv2Rgba(mBlobColorHsv);

        Log.i(TAG, "Touched rgba color: (" + mBlobColorRgba.val[0] + ", " + mBlobColorRgba.val[1] +
                ", " + mBlobColorRgba.val[2] + ", " + mBlobColorRgba.val[3] + ")");

        mDetector.setHsvColor(mBlobColorHsv);

        Imgproc.resize(mDetector.getSpectrum(), mSpectrum, SPECTRUM_SIZE);

        mIsColorSelected = true;

        touchedRegionRgba.release();
        touchedRegionHsv.release();

        return false; // don't need subsequent touch events
    }

    public Mat onCameraFrame(CvCameraViewFrame inputFrame) {
        mRgba = inputFrame.rgba();

        if (mIsColorSelected) {
            mDetector.process(mRgba);
            //GET QUADRILATERAL CONTOURS
            List<MatOfPoint> contours = mDetector.getContours();
            Log.e(TAG, "Contours count: " + contours.size());
            //GET CLOSEST CONTOUR TO CENTER
            Iterator<MatOfPoint> each = contours.iterator();
            MatOfPoint contour;
            Point central_point = new Point();
            central_point.x = mOpenCvCameraView.getWidth()/2;
            central_point.y = mOpenCvCameraView.getHeight()/2;
            Point ccontour = new Point();
            double center_x;
            double center_y;
            double current_dist;
            double min_dist=mOpenCvCameraView.getHeight()+mOpenCvCameraView.getWidth();
            List<MatOfPoint> central_contour = new ArrayList<MatOfPoint>();
            while (each.hasNext()){
            	contour = each.next();
            	center_x = (contour.toArray()[0].x+contour.toArray()[1].x+contour.toArray()[2].x+contour.toArray()[3].x)/4;
            	center_y = (contour.toArray()[0].y+contour.toArray()[1].y+contour.toArray()[2].y+contour.toArray()[3].y)/4;
            	current_dist = Math.sqrt((center_x-central_point.x)*(center_x-central_point.x) + (center_y-central_point.y)*(center_y-central_point.y));
            	if (current_dist < min_dist){
            		min_dist = current_dist;
            		central_contour.clear();
            		central_contour.add(contour);
            		ccontour.x = center_x;
            		ccontour.y = center_y;
            	}
            }
            Imgproc.drawContours(mRgba, contours, -1, CONTOUR_COLOR);
            Imgproc.drawContours(mRgba, central_contour,-1, CENTRAL_COLOR);
            
            //Draw and type direction of central contour
            if (!central_contour.isEmpty()){
            	if(first){
                    ScheduledExecutorService ex = Executors.newScheduledThreadPool(2);
                    ex.scheduleWithFixedDelay(new Runnable() {
                        @Override
                        public void run() {
                            try{
                                sDriver.write(("@"+Integer.toString(-currY)+":"+Integer.toString(currX)).getBytes("US-ASCII"),75);
                            } catch (IOException e){
                                //do nothing
                            }
                        }
                    }, 0, 100, TimeUnit.MILLISECONDS);
                    first = false;
                }
                Core.line(mRgba, ccontour, central_point, CENTRAL_COLOR, 5);
            	Core.putText(mRgba, "x = "+(ccontour.x-central_point.x), new Point(central_point.x*.1,central_point.y), Core.FONT_HERSHEY_COMPLEX, 1.0, CENTRAL_COLOR);
            	Core.putText(mRgba, "y = " + (central_point.y - ccontour.y), new Point(central_point.x * .1, central_point.y * 1.2), Core.FONT_HERSHEY_COMPLEX, 1.0, CENTRAL_COLOR);
                targX = scaledX(ccontour.x - central_point.x);
                targY = scaledY(central_point.y - ccontour.y);
            } else {
                targX = 0;
                targY = 0;
            }

            //UPDATE CURR VALUES WITH TARGET VALUES
            int diffX = targX - currX;
            int diffY = targY - currY;

            if(Math.abs(diffX) > 250){
                currX += (diffX/Math.abs(diffX))*250;
            } else {
                currX = targX;
            }

            if(Math.abs(diffY) > 250){
                currY += (diffY/Math.abs(diffY))*250;
            } else {
                currY = targY;
            }

            Mat colorLabel = mRgba.submat(4, 68, 4, 68);
            colorLabel.setTo(mBlobColorRgba);

            Mat spectrumLabel = mRgba.submat(4, 4 + mSpectrum.rows(), 70, 70 + mSpectrum.cols());
            mSpectrum.copyTo(spectrumLabel);
        }

        return mRgba;
    }

    private Scalar converScalarHsv2Rgba(Scalar hsvColor) {
        Mat pointMatRgba = new Mat();
        Mat pointMatHsv = new Mat(1, 1, CvType.CV_8UC3, hsvColor);
        Imgproc.cvtColor(pointMatHsv, pointMatRgba, Imgproc.COLOR_HSV2RGB_FULL, 4);

        return new Scalar(pointMatRgba.get(0, 0));
    }


    //USB SERIAL STUFF
    static void start(Context context, UsbSerialDriver driver) {
        sDriver = driver;
        final Intent intent = new Intent(context, ColorBlobDetectionActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_NO_HISTORY);
        context.startActivity(intent);
    }

    private void stopIoManager() {
        if (mSerialIoManager != null) {
            Log.i(TAG, "Stopping io manager ..");
            mSerialIoManager.stop();
            mSerialIoManager = null;
        }
    }

    private void startIoManager() {
        if (sDriver != null) {
            Log.i(TAG, "Starting io manager ..");
            mSerialIoManager = new SerialInputOutputManager(sDriver, mListener);
            mExecutor.submit(mSerialIoManager);
        }
    }

    private void onDeviceStateChange() {
        stopIoManager();
        startIoManager();
    }

    private void updateReceivedData(byte[] data) {
        String dataIn = hexStringToString(HexDump.toHexString(data));
        if(dataIn.startsWith("@")){
            dataIn = dataIn.replaceAll("@", "");
            if (dataIn.startsWith("i:")){
                yawText.setText(dataIn.replaceAll("i:", ""));
            } else if(dataIn.startsWith("m:")){
                modeText.setText(dataIn);
            } else {
                String[] temp = dataIn.split(":");
                if(temp.length == 2){
                    pitchText.setText("p:"+temp[0]);
                    yawText.setText("y:"+temp[1]);
                }
            }
        }
    }

    private String hexStringToString(String hex) {
        StringBuilder output = new StringBuilder();
        for (int i = 0; i < hex.length(); i+=2) {
            String str = hex.substring(i, i+2);
            output.append((char)Integer.parseInt(str, 16));
        }
        return output.toString();
    }

    private int scaledX(double in){
        return (int)((in/475)*4200);
    }
    private int scaledY(double in){
        return (int)((in/300)*3000);
    }
}
